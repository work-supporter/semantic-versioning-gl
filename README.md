# semantic-versioning-gl

study semantic versioning gitlab

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Setup Repo setting

Branch name in Push rules setting

```
(develop\/|production\/alpha|production\/beta|production\/stable|production\/(([0-9]+)\.)?([0-9]+)\.x)
```

Protected branches

* `main` Allowed to merge : `Maintainers`, Allowed to push : `No one`, Allowed to force push : `false`, Code owner approval : `false`
* `production/alpha` is same `main`
* `production/beta` is same `main`
* `production/stable` is same `main`
* `production/*.x` is same `main`

Protected tags

* `*` Allowed to create `Maintainers`

## tutorial

### step1. hello branching model and versioning
first, you create `develop/pre-alpha` branch from `main` branche. and check out `develop/pre-alpha`.

next, create file in repository root `study.txt`

edit `study.txt`. and commit

`study.txt`

```
semantic-versining
step 1
- feat: hello brenching model and versioning
```

`commit message`

```
feat: hello brenching model and versioning
```

push to origin.

create MR `develop/pre-alpha` to `main`.

create MR `main` to `production/alpha`.

make tag `v1.0.0-alpha.1` by gitlab ci 

### step2 publish production

you create `develop/step2` branch from `origin/main`. and publishing to origin. check out `develop/step2`.

edit `study.txt`. and commit

`study.txt`

```
semantic-versining
step 1
- feat: hello brenching model and versioning
step 2
- feat: publish production
```

`commit message`

```
feat: publish production
```

commit and push to origin.

create MR `develop/step2` to `main`.

create MR `main` to `production/alpha`.

make tag `v1.0.0-alpha.2` by gitlab ci 

create MR `main` to `production/beta`.

make tag `v1.0.0-beta.1` by gitlab ci 

create MR `main` to `production/stable`.

make tag `v1.0.0` by gitlab ci.

### step3

`develop/step3-1` branch from `origin/main`.
feat: validation uuid
feat: validation token
mr `main`, mr `production/alpha`
mr `main`, mr `production/beta`

`develop/step3-2` branch from `origin/main`.
feat: validation name
mr `main`, mr `production/alpha`

`develop/step3-3` branch from `origin/main`.
feat: validation key
mr `main`, mr `production/alpha`
mr `main`, mr `production/beta`
mr `main`, mr `production/stable`

## ref

https://github.com/semantic-release/semantic-release/blob/master/docs/usage/workflow-configuration.md
https://github.com/go-semantic-release/semantic-release
https://levelup.gitconnected.com/semantic-versioning-and-release-automation-on-gitlab-9ba16af0c21
https://www.conventionalcommits.org/en/v1.0.0/
https://tech.weperson.com/wedev/tech-stack-overview/